def transmogrify(string, options = {})
  defaults = {
    times: 5,
    upcase: false,
    reverse: false
  }

  options = defaults.merge(options)
  output = ''

  options[:times].times do
    output << string
  end
  output.upcase! if options[:upcase]
  output.reverse! if options[:reverse]

  output
end
